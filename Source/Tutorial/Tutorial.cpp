// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Tutorial.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Tutorial, "Tutorial" );

DEFINE_LOG_CATEGORY(LogTutorial)
 